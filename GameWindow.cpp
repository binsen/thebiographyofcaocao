/*
 * =====================================================================================
 *
 *       Filename:  GameWindow.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/13/2013 02:52:38 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include "GameWindow.h"

bool GameWindow::init( std::string title, int x, int y, int width, int height, int flags ){
    m_pWindow = SDL_CreateWindow( 
            title.c_str(), 
            x, 
            y, 
            width, 
            height, 
            flags
            );

    if( m_pWindow != NULL ){
        m_bMouseFocus = true;
        m_bKeyboardFocus = true;

        m_width = width;
        m_height = height;

        m_pRenderer = SDL_CreateRenderer( 
                m_pWindow, 
                -1, 
                SDL_RENDERER_ACCELERATED 
                );

        if( m_pRenderer == NULL ){
            printf( "Renderer could not be created! SDL Error: %s\n", 
                    SDL_GetError() );
            SDL_DestroyWindow( m_pWindow );
            m_pWindow = NULL;
            return false;
        }else{
            SDL_SetRenderDrawColor( m_pRenderer, 0x00, 0x00, 0x00, 0xFF );
            m_windowID = SDL_GetWindowID( m_pWindow );
            m_bShown = true;
        }
    }else{
        printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
        return false;
    }

    return true;
}

void GameWindow::render(void){
    /*
    SDL_SetRenderDrawColor( m_pRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
    SDL_RenderClear( m_pRenderer );
    SDL_RenderPresent( m_pRenderer );
    */
}

void GameWindow::update(void){

}

void GameWindow::handleEvents( SDL_Event &e ){
    if( e.type == SDL_WINDOWEVENT && e.window.windowID == m_windowID ){
        switch( e.window.event ){
            case SDL_WINDOWEVENT_SHOWN:
                m_bShown = true;
                break;
            case SDL_WINDOWEVENT_HIDDEN:
                m_bShown = false;
                break;
            case SDL_WINDOWEVENT_EXPOSED:
                SDL_RenderPresent( m_pRenderer );
                break;
            case SDL_WINDOWEVENT_ENTER:
                m_bMouseFocus = true;
                break;
            case SDL_WINDOWEVENT_LEAVE:
                m_bMouseFocus = false;
                break;
            case SDL_WINDOWEVENT_FOCUS_GAINED:
                m_bKeyboardFocus = true;
                break;
            case SDL_WINDOWEVENT_FOCUS_LOST:
                m_bKeyboardFocus = false;
                break;
            case SDL_WINDOWEVENT_CLOSE:
                SDL_HideWindow( m_pWindow );
                break;
        }
    }
}

void GameWindow::clean(void){
    if( m_pWindow != NULL ){
        SDL_DestroyWindow( m_pWindow );
    }

    m_bMouseFocus = false;
    m_bKeyboardFocus = false;
    m_width = 0;
    m_height = 0;
}

void GameWindow::focus(void){
}
