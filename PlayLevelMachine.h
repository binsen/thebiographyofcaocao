#ifndef __PLAYLEVELMACHINE_H__
#define __PLAYLEVELMACHINE_H__

#include <vector>
#include "PlayLevelMachine.h"

class PlayLevelMachine {
    public:

        PlayLevelMachine() {}
        ~PlayLevelMachine() {}

        void update();
        void render();
        void clean();

        void pushLevel( Level* pLevel );
        void changeLevel( Level* pLevel );
        void popLevel();

        std::vector<Level*>& getPlayLevels() { return m_playLevel; }

    private:
        std::vector<Level*> m_playLevels;
};

#endif
