#include "Game.h"
#include "TextureManager.h"
#include "InputHandler.h"
#include "MainMenuState.h"
#include "MenuButton.h"
#include "Role.h"
#include "GameObjectFactory.h"
#include "Background.h"
#include "SoundManager.h"

Game* Game::s_pInstance = 0;

Game::Game() :
m_bRunning(false)
{
    m_currentLevel = 1;
    m_levelFiles.push_back("Data/level1.xml");
    m_menuWindow = new GameWindow();
    m_mainWindow = new GameWindow();
}

Game::~Game(){
}

bool Game::init(
        const char* title, 
        int xpos, 
        int ypos, 
        int width, 
        int height, 
        bool fullscreen
        )
{ 
    int flags = 0;
    
    m_gameWidth = width;
    m_gameHeight = height;
    
    if(fullscreen)
        flags = SDL_WINDOW_FULLSCREEN;
    
    if(SDL_Init(SDL_INIT_EVERYTHING) == 0){
        std::cout << "SDL init success" << std::endl;
        m_mainWindow->init( title, xpos, ypos, width, height, flags );
        m_menuWindow->init( "", xpos+(width-200)/2, ypos+(height-200)/2, 200, 200, flags );
        m_windows.insert( std::pair<std::string,GameWindow*>( "main", m_mainWindow) );
        m_windows.insert( std::pair<std::string,GameWindow*>( "menu", m_menuWindow) );
    } else{
        std::cout << "SDL init fail" << std::endl;
        return false;
    }

    TheGameObjectFactory::Instance()->registerType(
            "Background", 
            new BackgroundCreator()
            );
    TheGameObjectFactory::Instance()->registerType(
            "MenuButton", 
            new MenuButtonCreator()
            );
    TheGameObjectFactory::Instance()->registerType(
            "Role", 
            new RoleCreator()
            );

    m_pGameStateMachine = new GameStateMachine();
    m_pGameStateMachine->changeState(new MainMenuState());

    m_bRunning = true;

    return true;
}

void Game::render(){
    std::map< std::string, GameWindow* >::iterator it = m_windows.begin();
    for( it=m_windows.begin(); it!=m_windows.end(); it++ )
        SDL_RenderClear( it->second->getRenderer() );

    m_pGameStateMachine->render();

    for( it=m_windows.begin(); it!=m_windows.end(); it++ )
        SDL_RenderPresent( it->second->getRenderer() );
}

void Game::update(){
    m_pGameStateMachine->update();
}

void Game::handleEvents(){
    TheInputHandler::Instance()->update();
}

void Game::clean(){
    m_windows.clear();
    
    SDL_Quit();
}
