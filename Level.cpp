//
//  Map.cpp
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 09/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#include "Level.h"
#include "TextureManager.h"
#include "Game.h"
#include <math.h>
#include <iostream>
/* 
#include "Layer.h"
#include "TileLayer.h"
 */

Level::~Level(){
    /* 
    for(int i = 0; i < m_layers.size(); i++){
        delete m_layers[i];
    }
    
    m_layers.clear();
 */
    for(int i = 0; i < m_gameObjects.size(); i++){
        delete m_gameObjects[i];
    }
    
    m_gameObjects.clear();
}

void Level::render(){
    /* 
    for(int i = 0; i < m_layers.size(); i++){
        m_layers[i]->render();
    } */
    for(int i = 0; i < m_gameObjects.size(); i++){
        m_gameObjects[i]->draw();
    }
}

void Level::update(){
    /* 
    for(int i = 0; i < m_layers.size(); i++){
        m_layers[i]->update(this);
    } */
    for(int i = 0; i < m_gameObjects.size(); i++){
        m_gameObjects[i]->update();
    }
}
