/*
 * =====================================================================================
 *
 *       Filename:  Role.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  02/14/2014 02:51:09 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <iostream>
#include "Role.h"
#include "TextureManager.h"
#include "Game.h"

Role::Role() : CaoObject() {
    m_alpha=0;
    count=0;
    maxcount=10;
}

void Role::load(std::unique_ptr<LoaderParams> const &pParams ){
    CaoObject::load(std::move(pParams));
    //m_scrollSpeed = pParams->getAnimSpeed();

    m_srcRect.x=0;
    m_destRect.x = m_position.getX();
    m_srcRect.y=0;
    m_destRect.y = m_position.getY();

    m_srcRect.w = m_destRect.w = m_srcRectWidth = m_destRectWidth = m_width;
    m_srcRect.h = m_destRect.h = m_height;
}

void Role::draw(){
    CaoObject::draw();
}

void Role::update(){
    CaoObject::update();
}

void Role::clean(){
    CaoObject::clean();
}
