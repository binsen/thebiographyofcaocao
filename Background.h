//
//  Background.h
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 26/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef __BACKGROUND_H__
#define __BACKGROUND_H__

#include <iostream>
#include "GameObjectFactory.h"
#include "CaoObject.h"

class Background : public CaoObject{
    public:

        virtual ~Background() {}
        Background();

        virtual void draw();
        virtual void update();
        virtual void clean();

        virtual void load(std::unique_ptr<LoaderParams> const &pParams);

    private:

        int m_scrollSpeed;

        int count;
        int maxcount;

        SDL_Rect m_srcRect;
        SDL_Rect m_destRect;

        int m_srcRectWidth;
        int m_destRectWidth;
};

class BackgroundCreator : public BaseCreator{
    public:
        virtual GameObject* createGameObject() const{
            return new Background();
        }
};

#endif /* defined(__Background_H__) */
