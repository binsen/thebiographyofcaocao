#ifndef __EXITSTATE_H__
#define __EXITSTATE_H__

#include <stdint.h>
#include <vector>
#include "MenuState.h"
#include "GameObject.h"

class ExitState : public GameState {
    public:
        virtual ~ExitState() {}

        virtual void update();
        virtual void render();

        virtual bool onEnter(); 
        virtual bool onExit(); 

        virtual std::string getStateID() const { return s_exitID; }
    private:
        static const std::string s_exitID;
        std::vector<GameObject*> m_gameObjects;
        Uint32 Start;
};

#endif
