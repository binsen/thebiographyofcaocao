//
//  Background.cpp
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 26/03/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#include "Background.h"
#include "TextureManager.h"
#include "Game.h"

Background::Background() : CaoObject() {
    m_alpha = 0;
    count = 0;
    maxcount = 10;
}

void Background::load(std::unique_ptr<LoaderParams> const &pParams){
    CaoObject::load(std::move(pParams));
    m_scrollSpeed = pParams->getAnimSpeed();
    
    m_srcRect.x = 0;
    m_destRect.x = m_position.getX();
    m_srcRect.y = 0;
    m_destRect.y = m_position.getY();
    
    m_srcRect.w = m_destRect.w = m_srcRectWidth = m_destRectWidth = m_width;
    m_srcRect.h = m_destRect.h = m_height;
}

void Background::draw(){
    SDL_RenderCopyEx(
            TheGame::Instance()->getRenderer("main"), 
            TheTextureManager::Instance()->getTextureMap()[m_textureID], 
            &m_srcRect, 
            &m_destRect, 
            0, 
            0, 
            SDL_FLIP_NONE
            );
}

void Background::update(){
}

void Background::clean(){
    CaoObject::clean();
}
