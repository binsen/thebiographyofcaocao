/*
 * =====================================================================================
 *
 *       Filename:  ExitState.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  11/01/2013 04:25:50 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */


#include <iostream>
#include "Game.h"
#include "ExitState.h"
#include "MainMenuState.h"
#include "TextureManager.h"
//#include "PlayState.h"
#include "InputHandler.h"
#include "StateParser.h"
#include "SoundManager.h"
#include "MenuButton.h"
#include <unistd.h>
#include <assert.h>

const std::string ExitState::s_exitID = "EXIT";

void ExitState::update(){
    if ( (SDL_GetTicks()-Start) > 1000 ){
        TheGame::Instance()->quit();
    }

    if(!m_gameObjects.empty()){
        for(int i = 0; i < m_gameObjects.size(); i++){
            if(m_gameObjects[i] != 0){
                m_gameObjects[i]->update();
            }
        }
    }
}

void ExitState::render(){
    if(m_loadingComplete && !m_gameObjects.empty()){
        for(int i = 0; i < m_gameObjects.size(); i++){
            m_gameObjects[i]->draw();
        }
    }
}

bool ExitState::onEnter( void ){
    std::cout << "Entering Exit State\n";
    std::cout << "\tReading configuration file ...";
    StateParser stateParser;
    stateParser.parseState(
            "assets/attack.xml", 
            s_exitID, 
            &m_gameObjects, 
            &m_textureIDList,
            &m_soundIDList
            );
    std::cout << "done" << std::endl;
    
    m_loadingComplete = true;
    std::cout << "Exit state init done!\n" << std::endl;

    Start = SDL_GetTicks();
    return true;
}

bool ExitState::onExit(){
    m_exiting = true;
    
    std::cout << "Exiting ExitState" << std::endl;
    std::cout << "\tDeleting objects ...";
    if(m_loadingComplete && !m_gameObjects.empty()){
        m_gameObjects.back()->clean();
        m_gameObjects.pop_back();
    }
    m_gameObjects.clear();
    std::cout << "done" << std::endl;

    std::cout << "\tClearing the texture manager ...";
    for(int i = 0; i < m_textureIDList.size(); i++){
        TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
    }
    std::cout << "done" << std::endl;
    
    std::cout << "\tReset input handler ...";
    TheInputHandler::Instance()->reset();
    std::cout << "done" << std::endl;
    std::cout << "Exit state exit!\n" << std::endl;
    
    return true;
}
