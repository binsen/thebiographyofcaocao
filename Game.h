#ifndef _GAME_H_
#define _GAME_H_

#include <map>
#include <SDL.h>
#include "GameStateMachine.h"
#include "GameWindow.h"

class Game {
    public:
        static Game* Instance(){
            if(s_pInstance == 0){
                s_pInstance = new Game();
            }
            return s_pInstance;
        }

        bool init(
                const char* title, 
                int xpos, 
                int ypos, 
                int width, 
                int height, 
                bool fullscreen
                );

        void render();
        void update();
        void handleEvents();
        void clean();

        SDL_Renderer* getRenderer( std::string id ) const { return m_windows.at(id)->getRenderer(); }
        GameStateMachine* getStateMachine() { return m_pGameStateMachine; }
        std::vector<std::string> getLevelFiles() { return m_levelFiles; }
        const int getCurrentLevel() { return m_currentLevel; }

        int getGameWidth() const { return m_gameWidth; }
        int getGameHeight() const { return m_gameHeight; }

        bool running() { return m_bRunning; }
        void quit() { m_bRunning = false; }
    private:
         static Game* s_pInstance;

         GameWindow* m_mainWindow;
         GameWindow* m_menuWindow;
         std::map< std::string, GameWindow* > m_windows;

         GameStateMachine* m_pGameStateMachine;
         std::vector<std::string> m_levelFiles;

         int m_gameWidth;
         int m_gameHeight;

         bool m_bRunning;

         int m_currentLevel;
         int m_nextLevel;

         Game();
         ~Game();
};

typedef Game TheGame;

#endif
