
#ifndef __ROLE_H__
#define __ROLE_H__ 

#include "GameObjectFactory.h"
#include "CaoObject.h"

class Role : public CaoObject {
    public:
        virtual ~Role(){}
        Role();

        virtual void draw();
        virtual void update();
        virtual void clean();

        virtual void load(std::unique_ptr<LoaderParams> const &pParams);

    private:
        int m_scrollSpeed;
        int count;
        int maxcount;

        SDL_Rect m_srcRect;
        SDL_Rect m_destRect;

        int m_srcRectWidth;
        int m_destRectWidth;
};

class RoleCreator : public BaseCreator {
    public:
        virtual GameObject* createGameObject() const {
            return new Role();
        }
};

#endif
