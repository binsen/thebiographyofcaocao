#ifndef __GAMEWINDOW_H__
#define __GAMEWINDOW_H__

#include <SDL.h>
#include <string>

class GameWindow {
public:
    GameWindow( void ){};

    int getWidth(void){ return m_width; }
    int getHeight(void){ return m_height; }

    bool isShown(void){ return m_bShown; }

    bool init(std::string title, int x, int y, int width, int height, int flags );
    void render(void);
    void update(void);
    void handleEvents( SDL_Event &e );
    void clean(void);
    void focus(void);
    SDL_Window* get_window(){ return m_pWindow; }
    SDL_Renderer* getRenderer() { return m_pRenderer; }

private:
    SDL_Window* m_pWindow;
    SDL_Renderer* m_pRenderer;

    int m_windowID;
    std::string m_GameWindowID; 

    int m_width;
    int m_height;

    bool m_bFullscreen;
    bool m_bMouseFocus;
    bool m_bKeyboardFocus;
    bool m_bShown;
};

#endif
