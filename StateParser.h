#ifndef __STATEPARSER_H__
#define __STATEPARSER_H__

#include <iostream>
#include <vector>
#include "tinyxml/tinyxml.h"

class GameObject;

class StateParser{
    public:

        StateParser() {}
        ~StateParser() {}

        bool parseState(
                const char* stateFile, 
                std::string stateID, 
                std::vector<GameObject*> *pObjects, 
                std::vector<std::string> *pTextureIDs,
                std::vector<std::string> *pSoundIDs
                );
    private:

        void parseObjects(
                TiXmlElement* pStateRoot, 
                std::vector<GameObject*> *pObjects
                );

        void parseTextures(
                TiXmlElement* pStateRoot, 
                std::vector<std::string> *pTextureIDs
                );

        void parseSounds(
                TiXmlElement* pSoundRoot,
                std::vector<std::string> *pSoundIDs
                );
};

#endif /* defined(__STATEPARSER_H__) */
