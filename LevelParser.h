#ifndef __LEVELPARSER_H__
#define __LEVELPARSER_H__

#include <iostream>
#include <vector>
#include "tinyxml.h"
#include "Level.h"

/* 
class Level;
class Layer;
*/

class LevelParser{
    public:
        Level* parseLevel(const char* levelFile);

    private:
        void parsePlot( TiXmlElement* pPlotRoot );
        void parseCombat( TiXmlElement* pCombatRoot );

        void parseTextures(TiXmlElement* pTextureRoot);
        void parseObjects(TiXmlElement* pObjectRoot);

        /* 
        void parseObjectLayer(
                TiXmlElement* pObjectElement, 
                std::vector<Layer*> *pLayers, 
                Level* pLevel
                );
 */

        int m_width;
        int m_height;

        Level* pLevel;
    
};

#endif /* defined(__LEVELPARSER_H__) */
