# - Find tinyxml2 source/include folder
# This module finds tinyxml2 if it is installed and determines where
# the files are. This code sets the following variables:
#
#  TINYXML2_FOUND             - have tinyxml2 been found
#  TINYXML2_INCLUDE_DIR       - path to where tinyxml2.h is found
#

find_package(PkgConfig Quiet)
pkg_check_modules(TinyXML2 QUIET tinyxml2)

find_path(TINYXML2_INCLUDE_DIR
    names tinyxml2.h
    hints
    ${TINYXML2_DIR}
    ${TINYXML2_INCLUDE_DIR}
    ${NSCP_INCLUDEDIR}
    )

find_library(TINYXML2_LIBRARY
    names tinyxml2
    hints
    ${TINYXML2_LIBDIR}
    ${TINYXML2_DIR}
    )

set(TINYXML2_INCLUDE_DIRS ${TINYXML2_INCLUDE_DIR})
set(TINYXML2_LIBRARIES ${TINYXML2_LIBRARY})

if(TINYXML2_INCLUDE_DIR)
    set(TINYXML2_FOUND TRUE)
else(TINYXML2_INCLUDE_DIR)
    set(TINYXML2_FOUND FALSE)
endif(TINYXML2_INCLUDE_DIR)

mark_as_advanced(
    TINYXML2_INCLUDE_DIR
    TINYXML2_LIBRARY
    )
